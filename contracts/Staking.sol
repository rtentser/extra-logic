//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract Staking {
    struct Stake {
        address staker;
        uint256 amount;
        uint256 interest;
        uint256 start;
        uint256 end;
    }

    address public owner;

    Stake[] public stakes;
    uint256 public totalStakes;

    uint16 public interestRate; // BPS
    mapping(uint256 => bool) public tariffs;
    uint16 public feeRate; // BPS
    address payable private feeReciever;

    uint256 private fees; // All accumulated fees

    event Deposit(
        uint256 indexed stakeID,
        address indexed staker,
        uint256 amount,
        uint256 fee,
        uint256 interest
    );
    event Withdraw(
        uint256 indexed stakeID,
        address indexed staker,
        uint256 amount,
        uint256 interest
    );

    modifier onlyOwner {
        require(msg.sender == owner, "!owner");
        _;
    }

    constructor(uint16 _initInterest, uint16 _initFee) {
        interestRate = _initInterest;
        feeRate = _initFee;

        tariffs[3] = true;
        tariffs[5] = true;
        tariffs[10] = true;

        owner = msg.sender;
    }

    function deposit(uint256 _amount, uint256 _duration) external payable {
        require(_amount > 0, "!deposit");
        require(tariffs[_duration], "!tariff");
        require(msg.value >= _amount, "!amount");

        uint256 _fee = (_amount * feeRate) / 10000;
        require(msg.value >= _amount + _fee, "!fee");

        uint256 _interest = (_amount * interestRate * _duration) / 10000;
        require(
            address(this).balance >= (_amount + _interest + _fee + fees),
            "!funds"
        );

        fees += _fee;
        totalStakes += _amount;
        stakes.push(
            Stake({
                staker: msg.sender,
                amount: _amount,
                interest: _interest,
                start: block.timestamp,
                end: block.timestamp + _duration * 60
            })
        );

        emit Deposit(stakes.length - 1, msg.sender, _amount, _fee, _interest);
    }

    function withdraw(uint256 stakeID) external {
        require(stakeID < stakes.length, "!length");
        require(stakes[stakeID].amount > 0, "!stake");
        require(msg.sender == stakes[stakeID].staker, "!staker");
        require(block.timestamp >= stakes[stakeID].end, "!end");

        uint256 amount = stakes[stakeID].amount;
        uint256 interest = stakes[stakeID].interest;

        delete stakes[stakeID];
        totalStakes -= amount;

        payable(msg.sender).transfer(
            amount + interest
        );
        emit Withdraw(
            stakeID,
            msg.sender,
            amount,
            interest
        );
    }

    function getStakesLength() external view returns (uint256) {
        return stakes.length;
    }

    function setInterestRate(uint16 _interestRate) external onlyOwner {
        require(_interestRate <= 10000, "!bps");
        interestRate = _interestRate;
    }

    function setTariff(uint256 _tariff, bool _status) external onlyOwner {
        tariffs[_tariff] = _status;
    }

    function setFeeRate(uint16 _feeRate) external onlyOwner {
        require(_feeRate <= 10000, "!bps");
        feeRate = _feeRate;
    }

    function getFeesAmount() external view onlyOwner returns (uint256) {
        return fees;
    }

    function getFeeReciever() external view onlyOwner returns (address) {
        return feeReciever;
    }

    function setFeeReciever(address payable _feeReciever) external onlyOwner {
        feeReciever = _feeReciever;
    }

    function retrieveFees() external onlyOwner {
        require(feeReciever != address(0), "!reciever");
        uint256 retrievingFees = fees;
        delete fees;
        feeReciever.transfer(retrievingFees);
    }

    receive() external payable {}

    fallback() external payable {}
}
