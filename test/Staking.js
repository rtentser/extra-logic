const Staking = artifacts.require("Staking");
const truffleAssert = require("truffle-assertions");
const utils = require("web3-utils");

contract("Staking", (accounts) => {
  let staking;
  let amount = "1000000000000000000";
  let term = 3;

  beforeEach(async () => {
    staking = await Staking.deployed();
  });

  it("should successfully deploy Staking", async () => {
    assert.equal(await staking.feeRate(), "10");
    assert.equal(await staking.interestRate(), "100");

    assert.isTrue(await staking.tariffs("3"));
    assert.isTrue(await staking.tariffs("5"));
    assert.isTrue(await staking.tariffs("10"));
  });

  it("should fail to deposit - zero amount", async () => {
    await truffleAssert.reverts(staking.deposit(0, term), "!deposit");
  });

  it("should fail to deposit - wrong tariff", async () => {
    await truffleAssert.reverts(staking.deposit(amount, term - 1), "!tariff");
  });

  it("should fail to deposit - not enough for amount", async () => {
    await truffleAssert.reverts(staking.deposit(amount, term), "!amount");
  });

  it("should fail to deposit - not enough for fee", async () => {
    await truffleAssert.reverts(
      staking.deposit(amount, term, { value: amount }),
      "!fee"
    );
  });

  it("should fail to deposit - not enough funds", async () => {
    await truffleAssert.reverts(
      staking.deposit(amount, term, { value: "1010000000000000000" }),
      "!funds"
    );
  });

  describe("should test successful deposit", async () => {
    let feesBefore;
    let interestRate;
    let feeRate;

    let stake;
    let fee;

    before(async () => {
      feesBefore = await staking.getFeesAmount();
      interestRate = await staking.interestRate();
      feeRate = await staking.feeRate();

      // send more to cover interest
      await staking.deposit(amount, term, { value: "2000000000000000000" });
      stake = await staking.stakes(0);
      fee = stake.amount.mul(feeRate).div(utils.BN(10000));
    });

    it("should create stake", async () => {
      assert.equal(stake.staker, accounts[0]);
      assert.equal(stake.amount, amount);
      assert.equal(
        stake.interest.toString(),
        interestRate
          .mul(utils.BN(amount))
          .mul(utils.BN(term))
          .div(utils.BN(10000))
          .toString()
      );
      assert.equal(
        stake.start.add(utils.BN(60 * term)).toString(),
        stake.end.toString()
      );
    });

    it("should emit Deposit event", async () => {
      let event = (await staking.getPastEvents("Deposit"))[0].returnValues;

      assert.equal(event.stakeID, 0);
      assert.equal(event.staker, stake.staker);
      assert.equal(event.amount, stake.amount);
      assert.equal(event.fee, fee);
      assert.equal(event.interest, stake.interest);
    });

    if (
      ("should increase fees",
      async () => {
        let feesAfter = await staking.getFeesAmount();
        assert.equal(feesBefore.add(fee), feesAfter);
      })
    )
      it("should fail to withdraw - out of boundaries", async () => {
        await truffleAssert.reverts(
          staking.withdraw(await staking.getStakesLength()),
          "!length"
        );
      });

    it("should fail to withdraw - sender isn't staker", async () => {
      await truffleAssert.reverts(
        staking.withdraw(0, { from: accounts[1] }),
        "!staker"
      );
    });

    it("should fail to withdraw - not ended", async () => {
      await truffleAssert.reverts(staking.withdraw(0), "!end");
    });

    describe("should test successful withdraw", async () => {
      let stake;
      let balanceBefore = accounts[0].balance;

      before(async () => {
        await increase((term + 1) * 60);
        stake = await staking.stakes(0);
        await staking.withdraw(0);
      });

      it("should emit Withdraw event", async () => {
        let event = (await staking.getPastEvents("Withdraw"))[0].returnValues;
        assert.equal(event.stakeID, 0);
        assert.equal(event.staker, accounts[0]);
        assert.equal(event.amount, stake.amount);
        assert.equal(event.interest, stake.interest);
      });

      it("should delete stake", async () => {
        let stakeAfter = await staking.stakes(0);
        assert.equal(stakeAfter.amount, 0);
      });

      it("should fail - already withdrawn", async () => {
        await truffleAssert.reverts(staking.withdraw(0), "!stake");
      });

      describe("test admin functions", async () => {
        it("should fail - not owner", async () => {
          await truffleAssert.reverts(
            staking.setInterestRate(200, { from: accounts[1] }),
            "!owner"
          );
          await truffleAssert.reverts(
            staking.setTariff(2, true, { from: accounts[1] }),
            "!owner"
          );
          await truffleAssert.reverts(
            staking.setFeeRate(200, { from: accounts[1] }),
            "!owner"
          );
          await truffleAssert.reverts(
            staking.getFeesAmount({ from: accounts[1] }),
            "!owner"
          );
          await truffleAssert.reverts(
            staking.setFeeReciever(accounts[1], { from: accounts[1] }),
            "!owner"
          );
          await truffleAssert.reverts(
            staking.retrieveFees({ from: accounts[1] }),
            "!owner"
          );
          await truffleAssert.reverts(
            staking.getFeeReciever({ from: accounts[1] }),
            "!owner"
          );
        });

        it("should change interest rate", async () => {
          await staking.setInterestRate(200);
          assert.equal(await staking.interestRate(), 200);
        });

        it("should change fee rate", async () => {
          await staking.setFeeRate(200);
          assert.equal(await staking.feeRate(), 200);
        });

        it("should change tariffs", async () => {
          await staking.setTariff(2, true);
          await staking.setTariff(10, false);
          assert.isTrue(await staking.tariffs(2));
          assert.isFalse(await staking.tariffs(10));
        });

        it("should fail - no fee reciever", async () => {
          await truffleAssert.reverts(staking.retrieveFees(), "!reciever");
        });

        it("should set fee reciever and retrieve fees", async () => {
          await staking.setFeeReciever(accounts[2]);
          assert.equal(await staking.getFeeReciever(), accounts[2]);

          let fees = await staking.getFeesAmount();
          let recieverBefore = await web3.eth.getBalance(accounts[2]);

          await staking.retrieveFees();
          let recieverAfter = await web3.eth.getBalance(accounts[2]);

          assert.equal(
            fees.add(utils.BN(recieverBefore)).toString(),
            utils.BN(recieverAfter).toString()
          );
        });

        it("should fail - not a rate", async () => {
          await truffleAssert.reverts(staking.setInterestRate(20000), "!bps");
          await truffleAssert.reverts(staking.setFeeRate(20000), "!bps");
        });
      });
    });
  });
});

async function increase(duration) {
  return new Promise((resolve, reject) => {
    web3.currentProvider.send(
      {
        jsonrpc: "2.0",
        method: "evm_increaseTime",
        params: [duration],
        id: new Date().getTime(),
      },
      (err, result) => {
        // second call within the callback
        web3.currentProvider.send(
          {
            jsonrpc: "2.0",
            method: "evm_mine",
            params: [],
            id: new Date().getTime(),
          },
          (err, result) => {
            // need to resolve the Promise in the second callback
            resolve();
          }
        );
      }
    );
  });
}
